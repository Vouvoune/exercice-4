package fr.cnam.foad.nfa035.dao;


import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;


public class BadgeWalletDAO {


    private File wallet;
    private ImageFileFrame media;

    public BadgeWalletDAO(String Filepath) throws IOException{
    this.wallet = new File(Filepath);
    this.media = new ImageFileFrame(wallet);
    }


    // Ajout badge
        public void addBadge (File image) throws IOException {

            ImageFileFrame media = new ImageFileFrame(wallet);

            // Sérialisation
            ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
            serializer.serialize(image, media);
        }

    // Lecture badge
        public void getBadge(OutputStream imageStream) throws IOException{



            ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
            deserializer.deserialize(media);


    }


}



